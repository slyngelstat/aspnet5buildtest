﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace AspNet5BuildTest.Tests
{


    public class SimpleTest
    {

        [Fact]
        public void Two_plus_Two_should_be_four()
        {
            var res = 2 + 2;

            Assert.Equal(4, res);
        }
    }
}
